# MineSweaper


1. Hozzuk létre a game.Minesweeper publikus osztályt!

2. Készítsünk egy createTable() metódust, mely paraméterül vár egy p valószínűséget és m, n egész számokat, és feltölt egy kétdimenziós, m sorból és n oszlopból álló logikai értékeket tartalmazó tömböt (a játéktáblát), ahol minden elem egy adott \(p\) valószínűséggel lesz logikai igaz. Ott lesznek az aknák, ahol az elem értéke logikai igaz.

Egy \(p\) eloszlású \(x\) indikátorváltozót a java.lang.Math.random() metódus segítségével tudunk előállítani:

boolean x (double p) {
  return Math.random() < p;
}
A fenti x metódust \(p\) valószínűséggel fog logikai igaz értéket, \(1-p\) valószínűséggel hamis értéket visszaadni.

3. Ez alapján készítsünk egy másik, solve() nevű függvényt, mely paraméterül vár egy aknakereső játéktáblát (kétdimenziós logikai tömböt), és visszaad egy egészekből álló kétdimenziós tömböt! A paraméter tömb és az eredmény tömb méretei azonosak.

Az eredmény tömbben egy elem azt jelöli, hogy a szomszédjában hány akna található. Ez lesz az aknakereső játék megoldása.

4. észítsünk egy-egy függvényt a logikai és a számokból álló tömb szöveggé alakítására! A függvények az átalakítandó tömböt paraméterül várják és szöveget adnak vissza.

Mindkét függvényben az akna helyére kerüljön csillag. A logikai tömbnél az aknamentes helyekre kerüljön pont.

A végeredmény hatékony előállításához használjuk a java.lang.StringBuffer osztályt.

5. Írjunk egy program.Main osztályt! Ebben helyet kap a main() metódus, mely egy sorban kéri be a szabványos bemenetről az aknakereső tábla méretét és a \(p\) valószínűséget! A szabványos kimenetre írjuk ki mindkét táblát!

A beolvasás megkönnyítésére használjuk a java.util.Scanner osztályt, és annak nextInt(), nextDouble() metódusait.
