package main;

import java.util.Scanner;

import game.Minesweeper;

public class Main {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		double possibility = sc.nextDouble();
		int m = sc.nextInt();
		int n = sc.nextInt();
		
		Minesweeper minesweeper = new Minesweeper();
		minesweeper.createTable(possibility, m, n);
		
		
		System.out.println(Minesweeper.fieldToString(minesweeper.getField()));
		
		System.out.println("\n");
		
		int[][] solvedField = minesweeper.solve();
		System.out.println(Minesweeper.solveToString(solvedField));//oszt�lyszint� fggvny
		
	}
}
