package game;

public class Minesweeper {
	private boolean field [][];
	
	
	public void createTable(double possibility, int m, int n) {
		this.field = new boolean[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if(Math.random() < possibility) {
					field[i][j] = true;
				}else {
					field[i][j] = false;
				}
			}
		}
		
	}
	
	public int[][] solve(){
		int[][] solveField = new int[field.length][field[0].length];
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[i].length; j++) {
				if(field[i][j] == true) {
					solveField[i][j] = -1;
				}else {
					if(i-1 >= 0
							&& field[i-1][j] 
							) {
							solveField[i][j]++;
					}
					if(i-1 >= 0
							&& j+1 < field[i].length
							&& field[i-1][j+1]
							) {
							solveField[i][j]++;
					}
					if(j+1 < field[i].length
							&& field[i][j+1]
							) {
							solveField[i][j]++;
					}
					if(i+1 < field.length
							&&j+1 < field[i].length
							&& field[i+1][j+1]
							) {
							solveField[i][j]++;	
					}
					if(i+1 < field.length
							&& field[i+1][j]
							) {
							solveField[i][j]++;
					}
					if(i+1 < field.length
							&& j-1 >= 0
							&& field[i+1][j-1]
							) {
							solveField[i][j]++;
					}
					if(j-1 > 0
							&& field[i][j-1]
							) {
							solveField[i][j]++;
					}
					if(i-1 > 0
							&&j-1 >= 0
							&& field[i-1][j-1]
							) {
							solveField[i][j]++;
					}
				}
			}
		}
		
		return solveField;
	}
	
	public static String solveToString(int[][] field) {
		StringBuilder solvedField = new StringBuilder();
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[i].length; j++) {
				if(field[i][j] == -1) {
					solvedField.append("*");
				}else {
					solvedField.append(field[i][j]);
				}
			}
			solvedField.append("\n");
		}
		return solvedField.toString();
	}
	
	public static String fieldToString(boolean[][] field) {
		StringBuilder solvedField = new StringBuilder();
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[i].length; j++) {
				if(field[i][j]) {
					solvedField.append("*");
				}else {
					solvedField.append(".");
				}
			}
			solvedField.append("\n");
		}
		return solvedField.toString();
	}

	public boolean[][] getField() {
		return field;
	}

	public void setField(boolean[][] field) {
		this.field = field;
	}
	
	
}
